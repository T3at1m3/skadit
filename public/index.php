<?php

require_once "../vendor/autoload.php";
define('DS', DIRECTORY_SEPARATOR);
define("APP_PATH", realpath(__DIR__));

use App\Core\Application;

$app = new Application();
$app->run();
