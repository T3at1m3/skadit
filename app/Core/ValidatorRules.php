<?php

namespace App\Core;

/**
 * The validator rules class
 * 
 * This class holds the rules for the validator
 *
 * @package skadit\App\Core
 * @author Al.Takev <aleksander.takev@Investor.bg>
 * @version 0.0.1
 */
class ValidatorRules
{
    private static $rules = array(
        'author_rules' => array(
            'author' => 'required',
        ),
        'book_rules'   => array(
            'title'     => 'required',
            'year'      => 'numeric'
        ),
    );
    public static function getRules($ruleName)
    {
        if (isset(self::$rules[$ruleName])) {
            return self::$rules[$ruleName];
        } else {
            return false;
        }
    }
}
