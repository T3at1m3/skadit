<?php

namespace App\Core;

/**
 * Main form input validator
 *
 * @package skadit\App\Core
 * @author Al.Takev <aleksander.takev@Investor.bg>
 * @version 0.0.1
 */
class Validator
{
    private static $formData;
    private static $errors = array();

    public static function check($formData, $rules)
    {
        self::$errors = array();
        $formData = self::sanitizeInput($formData);
        self::$formData = $formData;
        foreach( $formData as $key => $value ) {
            if(isset($rules[$key])) {
                $ruleArray = explode('|', $rules[$key]);
                foreach ($ruleArray as $rule) {
                    $ruleElement = explode(":", $rule, 2);
                    $method = $ruleElement[0];
                    unset($ruleElement[0]);
                    $ruleElement = array_values($ruleElement);
                    $status = self::$method($key, $value, $ruleElement);
                }
            }
        }
        return $status;
    }

    /**
     * Филтрира данните от input подадени от контролера
     * @param  array $formData
     * @return array
     */
    private static function sanitizeInput($formData)
    {
        $sanitizedData = filter_var_array($formData, FILTER_SANITIZE_STRING);
        return $sanitizedData;
    }

    private static function required($key, $value)
    {
        if(empty($value)) {
            self::$errors[$key]['required'] = 'Required field!';
            return false;
        } else
            return true;
    }

    public static function email($key, $value)
    {
        if(filter_var($value, FILTER_VALIDATE_EMAIL))
            return true;
        else {
            self::$errors[$key]['email'] = 'Must be valid email!';
            return false;
        }
    }

    /**
     * Проверка за полето парола
     * @param  string $value
     * @return boolean
     */
    private static function password($key, $value)
    {
        if(!preg_match('/^[a-zA-Z0-9]*([a-zA-Z][0-9]|[0-9][a-zA-Z])[a-zA-Z0-9]*$/', $value)) {
            self::$errors[$key]['password'] = 'The '.$key.' field: one number, one letter, no special chars!';
            return false;
        }
        else
            return true;
    }
    /**
     * Проверка дали полето е същото като това което е зададено след
     *  same:[fieldName]
     * @param  string $value
     * @param  string $validateData
     * @return boolean
     */
    private static function same($key, $value, $validateData)
    {
        if($value === self::$formData[$validateData[0]])
            return true;
        else {
            self::$errors[$key]['same'] = 'The field does not match the '.ucfirst($validateData[0]).' value';
            return false;
        }
    }

    /**
     * Checks for numeric values
     *
     * @param  string $value
     * @param  array  $validateData
     * @return boolean
     */
    private static function numeric($key, $value)
    {
        $status = is_numeric($value);
        if ($status == false) {
            self::$errors[$key]['numeric'] = 'This field must contain only numeric symbols!';
        }
        return $status;
    }

    public static function getErrors()
    {
        return self::$errors;
    }
}