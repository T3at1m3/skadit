<?php

namespace App\Core;

/**
 * The database class uses PDO MySQL connection
 *
 * @package skadit\App\Core
 * @author Al.Takev <aleksander.takev@Investor.bg>
 * @version 0.0.1
 */
class Database
{
    static $instance                = null;
    public static $connectionParams = array(
        'host'     => '127.0.0.1',
        'user'     => 'root',
        'pass'     => 'victory2233',
        'database' => 'catalogue',
        'charset'  => 'utf8',
    );
    private $pdo;
    private $stmt;
    private $sql;
    private $params;

    /**
     * Class constructor
     * @param array $connectionParamsParams The parameters needed for instantiating
     *                                a connection to the MySQL database
     */
    private function __construct()
    {
        $this->createConnection();
    }

    /**
     * The static instance of the class
     * @return obj
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function createConnection()
    {
        $dbOptions = array(
            \PDO::ATTR_PERSISTENT         => true,
            \PDO::MYSQL_ATTR_INIT_COMMAND => "set names " . self::$connectionParams['charset'],
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        );

        $dbconn = 'mysql:host=' . self::$connectionParams['host'] . ';dbname=' . self::$connectionParams['database'] . ';';
        $user   = self::$connectionParams['user'];
        $pass   = self::$connectionParams['pass'];
        try {
            $this->pdo = new \PDO($dbconn, $user, $pass, $dbOptions);
        } catch (Exception $e) {
            throw new \Exception("PDO Error: " . $e->getMessage());
        }
    }

    /**
     * The prepared statement to be executed by the PDO object
     * @param  string $sql    SQL statement
     * @param  array  $params Prepared parameters needed for the prepared
     *                        statement
     * @return object         The PDO object
     */
    public function prepare($sql, $params = array())
    {
        $this->stmt   = $this->pdo->prepare($sql);
        $this->params = $params;
        $this->sql    = $sql;
        return $this;
    }

    /**
     * The execute method, executes the prepared statement
     * @return object The PDO object
     */
    public function execute()
    {
        $this->stmt->execute($this->params);
        return $this;
    }

    /**
     * Returns the results of the executed query as an associative array
     * @param  boolean $assoc If the param is false, returns the array with
     *                        numerical values as keys
     * @return array
     */
    public function fetchAllAssoc($assoc = true)
    {
        if ($assoc) {
            return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            return $this->stmt->fetchAll(\PDO::FETCH_NUM);
        }
    }

    /**
     * Returns the result of the single executed query as an associative array
     * @return array
     */
    public function fetchRowAssoc($assoc = true)
    {
        if ($assoc) {
            return $this->stmt->fetch(\PDO::FETCH_ASSOC);
        } else {
            return $this->stmt->fetch(\PDO::FETCH_NUM);
        }

    }

    /**
     * Fetches the result based on the $column parameter
     * @param  string $column The desired column from a table in SQL
     * @return array         This array holds all the column data
     */
    public function fetchAllColumn($column)
    {
        return $this->stmt->fetchAll(\PDO::FETCH_COLUMN, $column);
    }

    /**
     * Fetches a single row of the selected column
     * @param  string $column The desired column from a table in SQL
     * @return array         The requested row
     */
    public function fetchRowColumn($column)
    {
        return $this->stmt->fetch(\PDO::FETCH_COLUMN, $column);
    }

    /**
     * The last inserted ID of an INSERT SQL query
     * @return string
     */
    public function lastInsertID()
    {
        return $this->pdo->lastInsertId();
    }

    /**
     * Counts the affected rows from the last query
     * @return integer The row count
     */
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }
}
