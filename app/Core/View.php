<?php

namespace App\Core;

/**
 * The main application class that handles the views
 * @package skadit\App\Core\
 * @author Al.Takev <aleksander.takev@Investor.bg>
 * @version 0.0.1
 */
class View
{
    private static $content;

    /**
     * @return the header content
     */
    public static function getHeader()
    {
        include 'header.html';
    }

    /**
     * @return the footer content
     */
    public static function getFooter()
    {
        include 'footer.html';
    }

    /**
     * Calls and sets the template for rendering
     * @param  string $module  The project directory
     * @param  string $filename The file
     * @param  array  $data     The data to be displayed into the file
     */
    public static function template($module, $filename, $data = array())
    {
        self::$content = self::render($module, $filename, $data);
    }

    /**
     * Sets the view which have to be displayed by the request
     * @param  string $module   Module directory
     * @param  string $filename The file
     * @param  array $data      The data which has to be displayed into the
     *                          view
     * @return the rendered view
     */
    public static function render($module, $filename, $data)
    {
        $filePath = '..' . DS . 'app' . DS . 'Modules' . DS .  $module . DS . 'views' . DS . $filename . '.html';

        if (!file_exists($filePath)) {
            throw new \Exception("No template file found " . $filePath);
        }

        if ($data) {
            extract($data);
        }

        ob_start();
        self::getHeader();
        include $filePath;
        self::getFooter();
        $content = ob_get_contents();

        ob_end_clean();

        echo $content;
    }
}
