<?php

namespace App\Core;

/**
 * The application initialiser class
 *
 * @package skadit\App\Core
 * @author Al.Takev <aleksander.takev@Investor.bg>
 * @version 0.0.1
 */
class Application
{
    /**
     * The method that initialises the whole application. The logic in it is
     * reinforced by the try catch block, that catches all exceptions generated
     * by the child classes
     */
    public function run()
    {
        Session::start();
        try {
            $url = $_SERVER['REQUEST_URI'];
            $router = new Router($url);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
