<?php

namespace App\Core;

/**
 * Application Router
 *
 * 2 main differences when deploying the application
 * When using http://localhost/<skadit>/
 *     the $url variable must have the trim($url, '/') function
 * else it's not needed
 * 
 * @package Skadit\App\Core;
 */
class Router
{
    private static $module;
    private static $controller;
    private static $method;
    private static $arg;
    private static $controllerInstance = array();

    public function __construct($url)
    {
        $url = explode('/', trim($url, '/'));
        /**
         * Unset the first URL part (remove the project name)
         */
        array_shift($url);
        $urlCount = count($url);

        $module = (isset($url[0]) && !empty($url[0])) ? ucfirst($url[0]) : 'User';
        self::$module = $module;

        /** @var string controller name */
        $controller = $module . 'Controller';
        self::$controller = $controller;
        /** @var string name of the controller's method */
        $method = !empty($url[1]) ? $url[1] : 'index';
        self::$method = $method;

        /** @var string arguments passed to the method */
        $arg = !empty($url[2]) ? $url[2] : null;
        if($urlCount > 3) {
            $arg = array();
            $arg[] = $url[2];
            $index = $urlCount - ($urlCount - 3);
            for($i = $index; $i < $urlCount; $i++) {
                $arg[] = $url[$i];
            }
        }

        self::$arg = $arg;

        Request::load($module, $controller, $method, $arg);
    }

    /**
     * Getters for the object properties
     */

    public static function getModule()
    {
        return self::$module;
    }
    public static function getController()
    {
        return self::$controller;
    }
    public static function getMethod()
    {
        return self::$method;
    }
    public static function getArg()
    {
        return self::$arg;
    }
}