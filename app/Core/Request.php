<?php

namespace App\Core;

/**
 * The application requester
 *
 * @package skadit\App\Core
 * @author Al.Takev <aleksander.takev@Investor.bg>
 * @version 0.0.1
 */
class Request
{
    private static $controllerInstance = array();

    private function __construct()
    {}

    /**
     * Instantiate proper controller with the requested method
     * @param  string $controller Controller name
     * @param  string $method     Method name
     * @param  string $arg        Arguments for the method
     * @return                    The Instance of the Controller + the method
     *                            that has to be called
     */
    public static function load($module, $controller, $method, $arg = null)
    {
        /** Check if the requested controller has been already instantiated */
        if (!isset(self::$controllerInstance[$controller])) {
            /** If the controller was not instantiated, try to ... */

            if (!class_exists($controller)) {
                $controller = "\App\Modules\\" . ucfirst($module) . '\\' . ucfirst($controller);
                if (!class_exists($controller)) {
                   throw new \Exception("Call to invalid controller class.");
                }
            }

            self::$controllerInstance[$controller] = new $controller;

            /** Check if the method of the controller can be called */
            if (!is_callable(array(self::$controllerInstance[$controller], $method))) {
                throw new \Exception('Call to invalid controller method.');
            }
        }
        /** The instance of the object with the call method */
        return self::$controllerInstance[$controller]->$method($arg);
    }
}