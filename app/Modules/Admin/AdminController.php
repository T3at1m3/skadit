<?php

namespace App\Modules\Admin;

use App\Core\Router;
use App\Core\View;

class AdminController
{
    private $module;

    public function __construct()
    {
        $this->module = Router::getModule();
    }

    public function index()
    {
        $data = array();
        View::template($this->module, 'home', $data);
    }
}