<?php

namespace App\Modules\Catalogue;

/**
 * This class is the Model for the Catalogue Module
 */
use App\Core\Database;
use App\Core\Validator;
use App\Core\ValidatorRules;

class Catalogue
{
    private $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    public function getDataByTable($table)
    {
        if ($table === 'books') {
            $sql = "SELECT b.*, a.name as author FROM {$table} as b
            LEFT JOIN authors as a ON b.author_id = a.id
            ORDER BY b.year DESC";
        } else {
            $sql = "SELECT a.*, COUNT(b.id) as book_count FROM {$table} as a
            LEFT JOIN books as b ON a.id = b.author_id 
            GROUP BY b.author_id
            ORDER BY id ASC";
        }

        $this->db->prepare($sql)->execute();
        $result = $this->db->fetchAllAssoc();
        return $result;
    }

    public function getByID($table, $id)
    {
        $sql    = "SELECT * FROM {$table} WHERE id = :id";
        $params = array(
            'id' => $id,
        );
        $this->db->prepare($sql, $params)->execute();
        $result = $this->db->fetchRowAssoc();
        return $result;
    }

    public function getByName($table, $search) 
    {   
        $sql = "SELECT b.*, a.name as author FROM {$table} as b
                LEFT JOIN authors as a ON b.author_id = a.id
                WHERE b.name LIKE '%{$search}%'
                ORDER BY b.year DESC";
        $this->db->prepare($sql)->execute();
        $result = $this->db->fetchAllAssoc();
        return $result;
    }

    public function add($table, $data)
    {
        switch ($table) {
            case 'authors':
                $rules = ValidatorRules::getRules('author_rules');
                $sql   = "INSERT INTO authors
                    (name, note)
                    VALUES
                    (:name, :note)";
                $params = array(
                    'name' => $data['author'],
                    'note' => $data['author-note'],
                );
                break;
            case 'books':
                $rules = ValidatorRules::getRules('book_rules');
                $sql   = "INSERT INTO books
                    (name, author_id, year, note, cover_url)
                    VALUES
                    (:name, :author_id, :year, :note, :cover_url)
                    ";
                $params = array(
                    'name'      => $data['title'],
                    'author_id' => $data['author_id'],
                    'year'      => $data['year'],
                    'note'      => $data['book_note'],
                    'cover_url' => $data['cover_url'],
                );
                break;
        }
        $validateData = Validator::check($data, $rules);
        if ($validateData === true) {
            $this->db->prepare($sql, $params)->execute();
            $id = $this->db->lastInsertID();
            if ($table === 'books') {
                $status = is_numeric($id) ? 'Book created!' : 'Cannot create book';
            } else {
                $status = is_numeric($id) ? 'Author created!' : 'Cannot create author';
            }
            $message['added'] = $status;
        } else {
            $message = Validator::getErrors();
        }
        return $message;
    }

    public function edit($table, $data, $id)
    {
        switch ($table) {
            case 'authors':
                $rules = ValidatorRules::getRules('author_rules');
                $sql   = "UPDATE {$table}
                        SET
                            name = :name,
                            note = :note
                        WHERE
                            id = :id";
                $params = array(
                    'id'   => $data['id'],
                    'name' => $data['author'],
                    'note' => $data['author-note'],
                );
                break;
            case 'books':
                $rules = ValidatorRules::getRules('book_rules');
                $sql   = "UPDATE {$table}
                        SET
                            name = :name,
                            author_id = :author_id,
                            year = :year,
                            note = :note,
                            cover_url = :cover_url
                        WHERE
                            id = :id";
                $params = array(
                    'id'        => $data['id'],
                    'name'      => $data['title'],
                    'author_id' => $data['author_id'],
                    'year'      => $data['year'],
                    'note'      => $data['book_note'],
                    'cover_url' => $data['cover_url'],
                );
        }
        $validateData = Validator::check($data, $rules);
        if ($validateData === true) {
            $this->db->prepare($sql, $params)->execute();
            $rowCount          = $this->db->rowCount();
            if ($table === 'books') {
                $message['edited'] = ($rowCount === 1) ? 'Book edited!' : 'No rows affected';
            } else {
                $message['edited'] = ($rowCount === 1) ? 'Author edited' : 'No rows affected';
            }
        } else {
            $message = Validator::getErrors();
        }
        return $message;
    }

    public function delete($table, $id)
    {
        $sql    = "SELECT name FROM {$table} WHERE id = :id";
        $params = array(
            'id' => $id,
        );
        $this->db->prepare($sql, $params)->execute();
        $name = $this->db->fetchRowColumn(0);
        if (!$name) {
            $data = "No such record exists";
        } else {
            $data = '<u>Author Name</u>: ' . $name . ' - Deleted!';
            $sql  = "DELETE FROM {$table} WHERE id = :id";
            $this->db->prepare($sql, $params)->execute();
        }

        return $data;
    }
}
