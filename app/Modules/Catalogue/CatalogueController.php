<?php

namespace App\Modules\Catalogue;

/**
 * The Catalogue Module - controller
 */
use App\Core\Router;
use App\Core\View;

class CatalogueController
{
    private $module;
    private $model;

    public function __construct()
    {
        $this->module = Router::getModule();
        $this->model = new Catalogue();
    }

    private function operations($arg, $template)
    {
        $table         = $template;
        $data['table'] = $this->model->getDataByTable($table);
        if($table === 'books') {
            $data['author_select'] = $this->model->getDataByTable('authors');
        }
        if ($arg === 'add') {
            $template = $template . '_operations';
            if ($_POST) {
                $data['message'] = $this->model->add($table, $_POST);
            }
        } else {
            $id = (int) $arg[1];
            switch ($arg[0]) {
                case 'edit':
                    $template      = $template . '_operations';
                    $data['input'] = $this->model->getByID($table, $id);
                    if ($_POST) {
                        $data['message'] = $this->model->edit($table, $_POST, $id);
                        $data['input']   = $this->model->getByID($table, $id);
                    }
                    break;
                case 'delete':
                    $data['message'] = $this->model->delete($table, $id);
                    $data['table']   = $this->model->getDataByTable($table);
                    break;
            }
        }
        View::template($this->module, $template, $data);
    }

    public function authors($arg)
    {
        $template = 'authors';
        $this->operations($arg, $template);
    }

    public function books($arg)
    {
        $template = 'books';
        if(isset($_POST['search']) && !empty($_POST['search'])) {
            $data['table'] = $this->model->getByName($template, $_POST['search']);
            View::template($this->module, $template, $data);
        } else {
            $this->operations($arg, $template);    
        }
    }
}
