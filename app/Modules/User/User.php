<?php

namespace App\Modules\User;

use App\Core\Database;

class User
{
    private $userData = array();
    private $db;

    public function __construct(array $user)
    {
        $this->userData = $user;
        $this->db = Database::getInstance();
    }

    public function checkUserLogin()
    {
        if(empty($this->userData['user']) && empty($this->userData['password'])) {
            $status = false;
        } else {
            $sql = "SELECT * FROM users WHERE username = :user AND password = :pass";
            $params = array( 'user' => $this->userData['user'], 'pass' => $this->userData['password']);
            $this->db->prepare($sql, $params)->execute();
            $result = $this->db->fetchRowAssoc();
            if($result['is_active'] === '1') {
                $status = $result;
            } else {
                $status = false;
            }
        }
        return $status;
    }
}