<?php

namespace App\Modules\User;

use App\Core\Router;
use App\Core\Session;
use App\Core\View;

/**
 * User Controller Class, handles all the user operations, and creates an 
 * instance of it's model
 */

class UserController
{
    /**
     * The main method
     */
    public function index()
    {
        View::template('User', 'default', array());
    }

    /**
     * When a user logs in
     */
    public function login()
    {
        $message  = array();
        $status   = false;
        $data     = array();
        $module   = Router::getModule();
        $template = 'login';
        if ($_POST) {
            $user   = new User($_POST);
            $status = $user->checkUserLogin();
            if (!$status) {
                $data['msg'] = 'No such user exists or fields must be populated';
            } else {
                Session::set('user_id', $status['id']);
                Session::set('username', $status['username']);
                header('Location: /skadit/admin/');
            }
        }

        View::template($module, $template, $data);
    }

    /**
     * When a user logs out
     */
    public function logout()
    {
        Session::destroy();
        header("Location: /skadit/");
    }
}