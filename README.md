# README #
Тестови проект по задача подадена за създаване на CRUD система.

### What is this repository for? ###

* CRUD system
* beta 0.1

### How do I get set up? ###

* Pull the repo.
* In folder /app/database/ you import the .sql file
* In app/Core/Database.php you insert your database options
* When using http://example.com/<project-name>/ the $url variable must have the trim($url, '/') function else it's not needed

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Процедура за login ###

1. Натискаме login и въвеждаме - user: billgates / pass: 123456
2. Изпраща ни към Admin страницата - няма никаква функционалност
3. От Listing-a има две опции за избор:
 Автор и Книги, а след това следват нещата по пуснатата задача: ADD / EDIT / DELETE